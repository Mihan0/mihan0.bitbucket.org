/* global $*/
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
var patientBMI=0;

/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 function klicGenerirajPodatke(){
	$('#obvestilaGenerirej').text("");
	generirajPodatke(1);
	generirajPodatke(2);
	generirajPodatke(3);
}
function generirajPodatke(stPacienta) {
  ehrId = "";
  
 if(stPacienta == 1){
		ehrId = "7dad6fbf-8ae6-4414-aca8-0f81143e44be"
		var tezz = 80;
		var viss = 190;
		dodajPodatkeEHR(ehrId,viss,tezz);
		$('#obvestilaGenerirej').append("</br>Mike Ivan: Višina: "+viss+"cm, Teža: "+tezz+"kg");
	}
	else if(stPacienta == 2){
		ehrId = "ef8e6d49-af85-49c6-ad49-2dd9a27161ba"
		var tezz = 130;
		var viss = 180;
		dodajPodatkeEHR(ehrId,viss,tezz);
		$('#obvestilaGenerirej').append("</br>Big Shaq: Višina: "+viss+"cm, Teža: "+tezz+"kg");
	}
	else if(stPacienta == 3){
		ehrId = "db3d3030-0a7f-4ad0-be50-71b9fcb19212"
		var tezz = 40;
		var viss = 170;
		dodajPodatkeEHR(ehrId,viss,tezz);
		$('#obvestilaGenerirej').append("</br>Lil Uzi: Višina: "+viss+"cm, Teža: "+tezz+"kg");
	}
  return ehrId;
}
  
  
function generiraj() {
    zacetek();
    var el = "<select id='selObj' class='form-control' onchange='spremeniPacienta(this.value)'>\
    <option value='0'>Izberi pacienta</option>\
    </select>";
    $("#patientSelectBtn").remove();
    $("#selDiv").empty();
    $("#selDiv").append(el);
    $("#status").html("Dodani ehrIdji: <br>");
    for (var i = 1; i <= 3; i++) {
      generirajPodatke(i, function(ehrId){
          generirajSelect(ehrId, function(opt) {
            $("#selObj").append(opt);
            $("#status").append(ehrId+"<br>");
        });
      });
    }
} 
  // TODO: Potrebno implementirati
function dodejPodatke(){
	var vis = $('#visina').val();
	var tez = $('#teza').val();
	var idd = $('#EHRID').val();
	dodajPodatkeEHR(idd,vis,tez);
}
function dodajPodatkeEHR(ehrId, visina, teza, sistol, distol){
	var sessid = getSessionId();
	$.ajaxSetup({
	headers: {
		"Ehr-Session":sessid
	}});
	var data = {
		"ctx/language": "en",
		"ctx/territory": "SI",
		"ctx/time": "2018-01-01",
		"vital_signs/height_length/any_event/body_height_length": visina,
        	"vital_signs/body_weight/any_event/body_weight": teza,
	};
	var parampampam = {
		ehrId: ehrId,
		templateId: 'Vital Signs',
		format: 'FLAT',
		committer: 'jst'
	};
	$.ajax({
		url: baseUrl + "/composition?" + $.param(parampampam),
		type: 'POST',
		contentType: 'application/json',
		data: JSON.stringify(data),
		success: function(odgovor){
			
	$('#obvestilaGenerirejj').text("Podatki dodani");
		},
		error: function(error){
	$('#obvestilaGenerirej').text("napaka");
			console.log("napaka"+error);
		}
	});
}
function dudejEHR(){
	dodejVBazo($('#dudejIme').val(),$('#dudejPriimek').val(),$('#dudejDatum').val());
}
function dodejVBazo(iime, ppriimek, ddatum){
	sessId = getSessionId();
	$.ajaxSetup({
	headers: {
		"Ehr-Session": sessId
	}});
	var odg = $.ajax({
		url: baseUrl + '/ehr',
		async: false,
		type: 'POST',
		success: function(data) {
			var ehrId = data.ehrId;
			var partyData = {
				firstNames: iime,
				lastNames: ppriimek,
				dateOfBirth: ddatum,
				partyAdditionalInfo: [{
					key: "ehrId", value: ehrId
				}]
		};
		$.ajax({
			url: baseUrl + "/demographics/party",
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(partyData),
			success: function(party){
				$('#obvestilaDudej').text('Vaš EHR ID: ' + ehrId);
			},
			error: function(error){
				$('#obvestilaDudej').text('napaka');
			}
		});
	}
	});
}
function izracunaj(){
    sessionId = getSessionId();
	var	iddd = $('#EHRIDDD').val();
	console.log(iddd);
	$('#tolePobris').text("");
	if (!iddd || iddd.trim().length == 0) {
		$('#tolePobris').text("manjka ER-HR ID");
		return;
	}
		$.ajax({
                    url: baseUrl + "/view/" + iddd + "/" + "weight" + iddd + "/" +"height",
                    type: 'GET',
                    headers: {
                        "Ehr-Session": sessionId
                    },
                    success: function(bmi){
                        $('#tolePobris').append("<p>Vaš BMI je: </p>" +patientBMI);
                    },
		
	error: function(){
		console.log("napaka");
	}
});
	}
	
function prikaziPodatke(bmi){
    patientBMI=Math.round(teza / (visina/100 * visina/100));
    
    if (patientBMI>0){
        $('#tolePobris').append("<p>Vaš BMI je: </p>" +patientBMI);
    }
}
$(window).load(function() {
    $("#vzorcnipacienti").change(function() {
        $('#EHRIDDD').val($('#vzorcnipacienti').val());
    });
});

  return ehrId;



// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
